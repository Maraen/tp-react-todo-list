# Base React

Ce projet est une base React simple avec le minimum de fichiers nécessaires pour faire quelque chose de propre.

## Installation

```bash
git clone https://git.octree.ch/crea/react/base react-base
cd react-base
npm install
```

## Utilisation

### Lancer le serveur de développement

```bash
npm start
```

Le serveur est accessible sur `http://localhost:3000`.
Quand on modifie les sources, la page est automatiquement rafraichie.

### Compiler les sources pour la production

```bash
npm run build
```

Une fois la commande terminée, on obtient un dossier `dist/` avec tous les
fichiers prêts à être publiés.

### Linter le code

```bash
npm run lint
```

Cette commande vérifie que le code respecte les règles de codage.

> Il est utile de mettre cette commande dans un Git Hook.

## Docker

### Construction de l'image

```bash
docker build -t app-react .
```

### Utilisation de l'image

```bash
docker run -d -p 8080:80 app-react
```

L'app est ensuite disponible sur `http://localhost:8080`.
