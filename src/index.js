import React from 'react';
import ReactDOM from 'react-dom';
import Storage from './components/storage';
import './styles/global.scss';

const ReactRoot = document.getElementById('react-root');

ReactDOM.render(<Storage />, ReactRoot);

module.hot.accept();
