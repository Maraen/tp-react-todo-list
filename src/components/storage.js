import React from 'react';
import localStorage from 'localStorage';

export default class Storage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      todos: [
        {text: '1', isCompleted: false},
        {text: '2', isCompleted: false},
        {text: '3', isCompleted: false},
      ],
    };

    //   this.handleChange = this.handleChange.bind(this);
    //   this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentDidUpdate() {
    localStorage.setItem('todos', JSON.stringify(this.state.todos));
  }

  componentDidMount() {
    const storedItems = JSON.parse(localStorage.getItem('todos'));
    const index = this.state.todos.length;
    console.log(index);
    if (storedItems != null) {
      this.setState({todos: storedItems});
    } else {
      this.setState({todos: this.state.todos});
      localStorage.setItem('todos', JSON.stringify(this.state.todos));
    }
  }

  setTodos = todos => this.setState({todos});
  setValue = value => this.setState({value});

  TodoForm = ({addTodo, index}) => {
    const handleSubmit = event => {
      event.preventDefault();
      const addTodo = text => {
        const newTodos = [...this.state.todos, {text}];
        this.setTodos(newTodos);
      };

      if (!this.state.value) return;
      addTodo(this.state.value);
      this.setValue('');
    };

    const clearAll = index => {
      let newTodos = [...this.state.todos];
      newTodos = [];
      this.setTodos(newTodos);
    };

    return (
      <form onSubmit={handleSubmit}>
        <label>Things to do</label>
        <input
          type="text"
          className="creationInput"
          name="thingtodo"
          value={this.state.value}
          onChange={event => this.setValue(event.target.value)}
        />
        <input className="btn" type="submit" value="Submit" />
        <button onClick={() => clearAll(index)}>Clear All</button>
      </form>
    );
  };

  Todo = ({todo, index}) => {
    const completeTodo = index => {
      const newTodos = [...this.state.todos];
      console.log(newTodos);
      console.log('1');
      newTodos[index].isCompleted = !newTodos[index].isCompleted;
      console.log(newTodos[index].isCompleted);
      console.log('2');
      this.setTodos(newTodos);
      console.log(this.state);
      console.log('3');
    };
    const removeTodo = index => {
      const newTodos = [...this.state.todos];
      newTodos.splice(index, 1);
      this.setTodos(newTodos);
    };
    return (
      <div className="todo" style={{backgroundColor: todo.isCompleted ? 'teal' : ''}}>
        {todo.text}
        <div>
          <button onClick={() => completeTodo(index)}>Complete</button>
          <button onClick={() => removeTodo(index)}>X</button>
        </div>
      </div>
    );
  };
  render() {
    return (
      <div className="container">
        <this.TodoForm addTodo={this.addTodo} />
        <ul>{this.listTestArray}</ul>
        {this.state.todos.map((todo, index) => (
          <this.Todo
            key={index}
            index={index}
            todo={todo}
            completeTodo={this.completeTodo}
            removeTodo={[this.removeTodo]}
          />
        ))}
      </div>
    );
  }
}
