FROM node:alpine

WORKDIR /app
COPY . /app

RUN npm ci && npm run build

FROM nginx:alpine

COPY --from=0 /app/dist /usr/share/nginx/html